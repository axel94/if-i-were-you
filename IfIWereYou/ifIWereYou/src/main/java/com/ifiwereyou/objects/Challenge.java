package com.ifiwereyou.objects;

import android.widget.TextView;

import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

@ParseClassName("Challenge")
public class Challenge extends ParseObject {

    public static final String KEY_CHALLENGE_TEXT = "challenge_text";
    public static final String KEY_CHALLENGER = "challenger";
    public static final String KEY_CHALLENGED = "challenged";

    public static final String KEY_ACCEPTED = "accepted";
    public static final String KEY_DECLINED = "declined";
    public static final String KEY_FULFILLED = "fulfilled";
    public static final String KEY_CANCELED = "canceled";

    private ChallengeState challengeState;

    public Challenge(){

    }

    public enum Type {
        OUTGOING,
        INCOMING
    }

    public void setChallengeText(String challengeText) {
        put(KEY_CHALLENGE_TEXT, challengeText);
    }

    public String getChallengeText() {
        return getString(KEY_CHALLENGE_TEXT);
    }

    public void setChallenger(ParseUser challenger) {
        put(KEY_CHALLENGER, challenger);
    }

    public ParseUser getChallenger() {
        return getParseUser(KEY_CHALLENGER);
    }

    public ChallengeState getChallengeState() {
        setChallengeState();
        return challengeState;
    }

    public void setChallenged(ParseUser challenged) {
        put(KEY_CHALLENGED, challenged);
    }

    public ParseUser getChallenged() {
        return getParseUser(KEY_CHALLENGED);
    }

    public boolean isFulfilled() {
        return getBoolean(KEY_FULFILLED);
    }

    public boolean isCancelled() {
        return getBoolean(KEY_CANCELED);
    }

    public boolean isOpen() {
        return isAccepted() && (!isFulfilled()) && (!isCancelled());
    }

    public boolean isAccepted() {
        return getBoolean(KEY_ACCEPTED);
    }

    public boolean isDeclined() {
        return getBoolean(KEY_DECLINED);
    }

    public boolean isNew() {
        return (!getBoolean(KEY_ACCEPTED)) && (!getBoolean(KEY_DECLINED));
    }

    public void accept() {
        if (!(challengeState.getState() == ChallengeState.state.NEW))
            return;
        String currentUserId = getCurrentUserId();
        if (getParseObject(KEY_CHALLENGED).getObjectId().equals(currentUserId)) {
            challengeState = new AcceptedChallenge();
            this.put(KEY_ACCEPTED, true);
            this.put(KEY_DECLINED, false);
        }
    }

    public void decline() {
        if (!(challengeState.getState() == ChallengeState.state.NEW))
            return;
        String currentUserId = getCurrentUserId();
        if (getParseObject(KEY_CHALLENGED).getObjectId().equals(currentUserId)) {
            challengeState = new DeclinedChallenge();
            this.put(KEY_DECLINED, true);
            this.put(KEY_ACCEPTED, false);
        }
    }

    public void fulfill() {
        if (!(challengeState.getState() == ChallengeState.state.ACCEPTED))
            return;
        String currentUserId = getCurrentUserId();
        if (getParseObject(KEY_CHALLENGED).getObjectId().equals(currentUserId)) {
            challengeState = new FulfilledChallenge();
            this.put(KEY_FULFILLED, true);
            this.put(KEY_CANCELED, false);
        }
    }

    public void cancel() {
        if (!(challengeState.getState() == ChallengeState.state.ACCEPTED))
            return;
        String currentUserId = getCurrentUserId();
        if (getParseObject(KEY_CHALLENGED).getObjectId().equals(currentUserId)) {
            challengeState = new CanceledChallenge();
            this.put(KEY_CANCELED, true);
            this.put(KEY_FULFILLED, false);
        }
    }

    public String getCurrentUserId() {
        return ParseUser.getCurrentUser().getObjectId();
    }

    public String getParseUserObjectId(ParseUser parseUser) {
        return parseUser.getObjectId();
    }

    public boolean currentUserIsChallenger() {
        String currentUserId = getCurrentUserId();
        return getChallenger().getObjectId().equals(currentUserId) && !getChallenged().getObjectId().equals(currentUserId);
    }

    public boolean currentUserIsChallenged() {
        String currentUserId = getCurrentUserId();
        return getChallenged().getObjectId().equals(currentUserId) && !getChallenger().getObjectId().equals(currentUserId);
    }

    public ParseUser getMyOpponent() {
        return (currentUserIsChallenger()) ? getChallenged()
                : (currentUserIsChallenged()) ? getChallenger()
                : null;
    }

    public Type getType() {
        return (currentUserIsChallenger()) ? Type.OUTGOING
                : (currentUserIsChallenged()) ? Type.INCOMING
                : null;
    }

    public void setChallengeState() {
        if (isNew()) {
            challengeState = new NewChallenge();
        } else if (isDeclined()) {
            challengeState = new DeclinedChallenge();
        } else if (isOpen()) {
            challengeState = new AcceptedChallenge();
        } else {
            if (isFulfilled()) {
                challengeState = new FulfilledChallenge();
            } else if (isCancelled()) {
                challengeState = new CanceledChallenge();
            }
        }
    }

    public void printStatus(TextView statusTextView) {

        setChallengeState();
        challengeState.printStatus(statusTextView);
    }

    public static ParseQuery<Challenge> getQuery() {
        return ParseQuery.getQuery(Challenge.class);
    }

}