package com.example.myapplication;

import com.ifiwereyou.BuildConfig;
import com.ifiwereyou.objects.Challenge;
import com.ifiwereyou.objects.Friendship;
import com.parse.ParseUser;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import static org.junit.Assert.assertNotNull;

/**
 * Created by D060426 on 08.06.2015.
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, manifest = "build/intermediates/manifests/debug/AndroidManifest.xml", resourceDir = "../../../../build/intermediates/res/debug" , emulateSdk = 18, reportSdk = 18)
/**
 * Created by D060336 on 6/11/2015.
 */
public class FriendshipObjectTest {

    private Friendship object;

    @Before
    public void createObject(){
        object = new Friendship();
    }

    @Test
    public void testObjectNotNull(){
        assertNotNull(object);
    }

    @Test
    public void testmethods(){
        object.getFriendA();
        object.getFriendB();
    }
}
