package com.example.myapplication;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;

import com.ifiwereyou.BuildConfig;
import com.ifiwereyou.R;
import com.ifiwereyou.activities.AddContactActivity;
import com.ifiwereyou.fragments.AddContactFragment;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by D060426 on 08.06.2015.
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, manifest = "build/intermediates/manifests/debug/AndroidManifest.xml", resourceDir = "../../../../build/intermediates/res/debug" , emulateSdk = 18, reportSdk = 18)
public class AddContactFragmentTest {

    private AddContactActivity addContactActivity;
    private AddContactFragment addContactFragment;

    @Before
    public void setup() {
        addContactActivity = Robolectric.buildActivity(AddContactActivity.class).create()
                .start().resume()
                .get();
        addContactFragment = new AddContactFragment();
        startFragment(addContactFragment, addContactActivity);
    }

    @Test
    public void checkFragmentNotNull() throws Exception {
        assertNotNull(addContactFragment);
        addContactFragment.outputToastMessage(R.string.add_contact_email_not_valid_message);
        addContactFragment.add();
    }

    @Test
    public void checkValidMailMethod() throws Exception {
        assertTrue(addContactFragment.isValidEmail("test1@test.de"));
    }

    @Test
    public void checkValidMailMethodFalse() throws Exception {
        assertFalse(addContactFragment.isValidEmail("test1"));
    }

    public void startFragment(Fragment fragment, ActionBarActivity activity){
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(fragment,null);
        fragmentTransaction.commit();
    }


}

