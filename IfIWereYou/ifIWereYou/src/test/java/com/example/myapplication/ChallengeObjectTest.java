package com.example.myapplication;

import com.ifiwereyou.objects.Challenge;
import com.ifiwereyou.BuildConfig;
import com.parse.Parse;
import com.parse.ParseUser;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

/**
 * Created by D060426 on 08.06.2015.
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, manifest = "build/intermediates/manifests/debug/AndroidManifest.xml", resourceDir = "../../../../build/intermediates/res/debug" , emulateSdk = 18, reportSdk = 18)
/**
 * Created by D060336 on 6/11/2015.
 */
public class ChallengeObjectTest {

    private Challenge object;

    @Before
    public void createObject(){
        object = new Challenge();
    }

    @Test
    public void testObjectNotNull(){
        assertNotNull(object);
    }

    @Test
    public void testMethods(){
        object.getChallengeState();
        object.getChallenged();
        object.getChallenger();
        object.getChallengeText();
        object.setChallengeText("test");
        object.setChallenged(new ParseUser());
        object.setChallenger(new ParseUser());
    }

    @Test
    public void testChallengeType(){
        Challenge.Type.values();
        Challenge.Type.valueOf("INCOMING");
        Challenge.Type.valueOf("OUTGOING");
    }
}
