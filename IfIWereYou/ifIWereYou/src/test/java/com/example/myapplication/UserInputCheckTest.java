package com.example.myapplication;
import com.ifiwereyou.IfIWereYouApplication;
import com.ifiwereyou.objects.Challenge;
import com.ifiwereyou.BuildConfig;
import com.ifiwereyou.utils.UserInputCheck;
import com.parse.Parse;
import com.parse.ParseUser;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by D060336 on 6/11/2015.
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, manifest = "build/intermediates/manifests/debug/AndroidManifest.xml", resourceDir = "../../../../build/intermediates/res/debug" , emulateSdk = 18, reportSdk = 18)
public class UserInputCheckTest {

    @Before
    public void createUtil(){
        new UserInputCheck();
    }

    @Test
    public void checkValidMailMethod() {
        IfIWereYouApplication.getContext();
        assertTrue(UserInputCheck.isValidEmail("test1@test.de"));
    }

    @Test
    public void checkValidMailMethodFalse() {
        IfIWereYouApplication.getContext();
        assertFalse(UserInputCheck.isValidEmail("test"));
    }

}
