package com.example.myapplication;

import com.ifiwereyou.BuildConfig;
import com.ifiwereyou.objects.AcceptedChallenge;
import com.ifiwereyou.objects.CanceledChallenge;
import com.ifiwereyou.objects.Challenge;
import com.ifiwereyou.objects.ChallengeState;
import com.ifiwereyou.objects.DeclinedChallenge;
import com.ifiwereyou.objects.FulfilledChallenge;
import com.parse.ParseUser;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import static org.junit.Assert.assertNotNull;

/**
 * Created by D060426 on 08.06.2015.
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, manifest = "build/intermediates/manifests/debug/AndroidManifest.xml", resourceDir = "../../../../build/intermediates/res/debug" , emulateSdk = 18, reportSdk = 18)
/**
 * Created by D060336 on 6/11/2015.
 */
public class ChallengeStateObjectTest {

    private ChallengeState object;
    private ChallengeState object2;
    private ChallengeState object3;
    private ChallengeState object4;

    @Before
    public void createObject(){
        object = new DeclinedChallenge();
        object2= new AcceptedChallenge();
        object3 = new FulfilledChallenge();
        object4 = new CanceledChallenge();
    }

    @Test
    public void testObjectNotNull(){

        assertNotNull(object);
        assertNotNull(object2);
        assertNotNull(object3);
        assertNotNull(object4);
    }

    @Test
    public void testMethods(){
        object.getIncomingViewType();
        object.getState();
        object2.getIncomingViewType();
        object2.getState();
        object3.getIncomingViewType();
        object3.getState();
        object4.getIncomingViewType();
        object4.getState();
    }
}
