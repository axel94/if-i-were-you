package com.example.myapplication;

import android.view.View;
import android.view.ViewGroup;

import com.ifiwereyou.BuildConfig;
import com.ifiwereyou.IfIWereYouApplication;
import com.ifiwereyou.activities.MainActivity;
import com.ifiwereyou.objects.Challenge;
import com.ifiwereyou.provider.ChallengeParseQueryAdapter;
import com.parse.ParseUser;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import static org.junit.Assert.assertNotNull;

/**
 * Created by D060426 on 08.06.2015.
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, manifest = "build/intermediates/manifests/debug/AndroidManifest.xml", resourceDir = "../../../../build/intermediates/res/debug" , emulateSdk = 18, reportSdk = 18)
public class ChallengeParseQueryAdapterTest {

    private ChallengeParseQueryAdapter adapter;

    @Before
    public void setup() {
        adapter = new ChallengeParseQueryAdapter(IfIWereYouApplication.getContext(), new ParseUser());
    }

    @Test
    public void checkNextActivityNotNull() throws Exception {
        assertNotNull(adapter);
    }

    @Test
    public void checkGetItemView()throws  NullPointerException{
        adapter.canCurrentUserSendNewChallenge();
    }
}